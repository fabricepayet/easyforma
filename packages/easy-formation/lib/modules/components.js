import '../components/login/Login.jsx';
import '../components/sessions/SessionsList.jsx';

// Promos components
import '../components/promos/PromosList.jsx';
import '../components/promos/PromosItem.jsx';
import '../components/promos/PromosNewForm.jsx';
import '../components/promos/PromosEditForm.jsx';

// Rooms
import '../components/rooms/Room.jsx';

// Common
import '../components/common/Header';
import '../components/common/Layout';

// Formateurs
import '../components/formateurs/FormateursList.jsx';
import '../components/formateurs/FormateurItem.jsx';
import '../components/formateurs/FormateurNewForm.jsx';
