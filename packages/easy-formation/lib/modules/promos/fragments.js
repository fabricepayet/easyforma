import { registerFragment } from 'meteor/vulcan:core';

registerFragment(`
  fragment PromosItemFragment on Promo {
    _id
    createdAt
    userId
    user {
      displayName
    }
    name
    year
  }
`);

registerFragment(`
  fragment PromosRoomFragment on Promo {
    _id
    createdAt
    formateurs {
      firstname
      lastname
    }
    name
    year
  }
`)
