import { addRoute } from 'meteor/vulcan:core';

addRoute({ name: 'login', path: '/', componentName: 'PromosList' })
addRoute({ name: 'room', path: '/rooms/:promoId', componentName: 'Room' })
addRoute({ name: 'formateurs', path: '/formateurs', componentName: 'FormateursList' })
