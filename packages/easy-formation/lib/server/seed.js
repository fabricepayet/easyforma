import Promos from '../modules/promos/collection.js';
import Formateurs from '../modules/formateurs/collection.js';
import Users from 'meteor/vulcan:users';
import { createMutator } from 'meteor/vulcan:core';

const PromoSeedData = [
  {
    name: 'Formation développeur',
    year: 2018,
  },
  {
    name: 'Formation Community Manager',
    year: 2018,
  },
  {
    name: 'Formation Producteur vidéo',
    year: 2018,
  },
  {
    name: 'Formation marketing numérique',
    year: 2018,
  },
  {
    name: 'Formation développeur',
    year: 2017,
  },
  {
    name: 'Formation Community Manager',
    year: 2017,
  },
  {
    name: 'Formation Producteur vidéo',
    year: 2017,
  },
  {
    name: 'Formation marketing numérique',
    year: 2017,
  },
  {
    name: 'Formation développeur',
    year: 2016,
  },
  {
    name: 'Formation Community Manager',
    year: 2016,
  },
  {
    name: 'Formation Producteur vidéo',
    year: 2016,
  },
  {
    name: 'Formation marketing numérique',
    year: 2016,
  },
];

FormateursSeedData = [
  {
    firstname: 'Djothi',
    lastname: 'Grondin',
    description: 'Sérieux et volontaire, l\'informatique est plus qu\'un métier pour moi.C\'est une passion pour laquelle j’œuvre à travers mes études, mes projets et mes initiatives personnelles.Par ailleurs, j\'ai la conviction que la dimension humaine est primordiale dans le monde du travail permettant ainsi de rendre une équipe forte, efficace et motivée.Cette conviction m\'a permis de m’adapter à mes environnements de travail et ce quelles que soient les problématiques qui m\'ont été confiées. "I\'m convinced that the only thing that kept me going was that I loved what I did" Steve Jobs'
  },
  {
    firstname: 'Fabrice',
    lastname: 'Payet',
    description: "Bonjour, je suis Fabrice Payet et j'aide les porteurs de projets et startups dans la création de leur produit. En tant que consultant et développeur fullstack, je m'assure des bons choix techniques et du développement de votre application. Je m'inspire des méthodologie Scrum et Lean Startup, pour livrer régulièrement et rapidement de nouvelles fonctionnalités. Mon objectif est de construire au plus tôt le produit qui correspondra à vos utilisateurs."
  }, {
    firstname: 'Clair',
    lastname: 'Alexandre',
    description: '💡Entrepreneur dans le digital et passionné par le design.'
  }
]

Meteor.startup(function () {
  if (Users.find().fetch().length === 0) {
    Accounts.createUser({
      username: 'DemoUser',
      email: 'dummyuser@telescopeapp.org',
      profile: {
        isDummy: true
      }
    });
  }

  const currentUser = Users.findOne();
  if (Promos.find().fetch().length === 0) {
    PromoSeedData.forEach(document => {
      createMutator({
        collection: Promos,
        document: document,
        currentUser: currentUser,
        validate: false
      });
    });
  }

  if (Formateurs.find().fetch().length === 0) {
    FormateursSeedData.forEach(document => {
      createMutator({
        collection: Formateurs,
        document: document,
        currentUser: currentUser,
        validate: false
      });
    });
  }
});
