import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, getFragment } from "meteor/vulcan:core";

import Promos from '../../modules/promos/collection.js';

const PromosEditForm = ({ documentId, closeModal }) =>

  <Components.SmartForm
    collection={Promos}
    documentId={documentId}
    mutationFragment={getFragment('PromosItemFragment')}
    showRemove={true}
    successCallback={document => {
      closeModal();
    }}
  />

registerComponent({ name: 'PromosEditForm', component: PromosEditForm });
