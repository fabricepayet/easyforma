import React, { PropTypes, Component } from 'react';
import { registerComponent, Components } from 'meteor/vulcan:core';

const FormateurItem = ({ formateur }) =>
  <div>
    <h2>{formateur.firstname} {formateur.lastname}</h2>
    <p>{formateur.description}</p>
  </div>


registerComponent({ name: 'FormateurItem', component: FormateurItem });
