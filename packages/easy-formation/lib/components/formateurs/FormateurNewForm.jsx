import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, withCurrentUser, getFragment } from 'meteor/vulcan:core';

import Formateurs from '../../modules/formateurs/collection.js';

const FormateurNewForm = ({ currentUser }) =>

  <div>
    {
      Formateurs.options.mutations.create.check(currentUser) ?
        <div style={{ marginBottom: '20px', paddingBottom: '20px', borderBottom: '1px solid #ccc' }}>
          <h4>Ajouter un formateur</h4>
          <Components.SmartForm
            collection={Formateurs}
            mutationFragment={getFragment('FormateursItemFragment')}
          />
        </div> :
        null
    }
  </div>

registerComponent({ name: 'FormateurNewForm', component: FormateurNewForm, hocs: [withCurrentUser] });
