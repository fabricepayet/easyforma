import React, { PropTypes, Component } from 'react';
import { Components, registerComponent, withMulti, withCurrentUser, Loading } from "meteor/vulcan:core";

import Formateurs from '../../modules/formateurs/collection';

const FormateursList = ({ results = [], currentUser, loading, loadMore, count, totalCount }) =>
  <div style={{ maxWidth: '500px', margin: '20px auto' }}>


    {loading ?

      <Loading /> :

      <div className="formateurs">
        {
          Formateurs.options.mutations.create.check(currentUser) ?
            <Components.ModalTrigger label="Ajouter un formateur">
              <Components.FormateurNewForm />
            </Components.ModalTrigger>
            : null
        }
        <hr />
        <h1>Mes formateurs</h1>
        {results.map(formateur => <Components.FormateurItem key={formateur._id} formateur={formateur} currentUser={currentUser} />)}

        {totalCount > results.length ?
          <a href="#" onClick={e => { e.preventDefault(); loadMore(); }}>Voir plus ({count}/{totalCount})</a> : ''
        }
      </div>
    }

  </div>

const options = {
  collection: Formateurs,
  fragmentName: 'FormateursItemFragment',
  limit: 5
};

registerComponent({ name: 'FormateursList', component: FormateursList, hocs: [[withMulti, options], withCurrentUser] });
